import React from "react";

import docker from "../images/docker.svg";
import cloud from "../images/cloud.svg";
import terminal from "../images/terminal.svg";
import fire from "../images/fire.svg";
import gear from "../images/gear.svg";
import screenshot from "../images/app-screenshot.png";
import endpointScreenShot from "../images/endpoint.png";
import smartAppBuilderScreenshot from "../images/smart-app-builder2.png";
import dndFeatureScreenshot from "../images/dnd-feature.png";
import deploymentFeatureScreenshot from "../images/deployment.png";
import terminologyScreenshot from "../images/terminology.png";
import questionnaireScreenshot from "../images/questionnaire.png";
import fhirpathFeatureScreenshot from "../images/fhirpath-editor.png";
import fhirpathFeatureScreenshot2 from "../images/fhirpath-editor2.png";

import { ImageCard } from "../components/ImageCard";
import {
  Section,
  ContentSection,
  SectionTitle,
  SectionContentContainer,
  SectionContent,
} from "../components/Section";
import { VideoHeader } from "../components/VideoHeader";

export default function Home() {
  return (
    <div className="border-slate-900">
      <VideoHeader
        headerClass="text-blue-900"
        title="Create modern healthcare apps fast."
        subTitle="Create, Deploy and Test FHIR based applications"
        videoSrc="https://www.youtube.com/embed/yYdsiu-_zGw"
      />
      <div className="mt-8 mb-16 flex items-center flex-col">
        <div className=" flex justify-center items-center space-x-3">
          <div className="inline-flex rounded-md shadow">
            <a
              href="https://workspace.genfhi.app"
              target="_blank"
              className="text-xl font-bold inline-flex items-center justify-center space-x-2 px-4 py-2 border border-transparent rounded-md text-white bg-orange-600 hover:bg-orange-700"
            >
              <span>Sign up</span>{" "}
              <span className="hidden lg:block">for cloud workspace</span>
            </a>
          </div>
          <div className="inline-flex">
            <a
              href="https://gitlab.com/genfhi/genfhi/container_registry/2417387"
              className="text-xl inline-flex items-center justify-center space-x-2 px-4 py-2 border border-transparent rounded-md text-orange-700 bg-orange-100 hover:bg-orange-200"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
                className="w-5 h-5"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"
                ></path>
              </svg>
              <span>Download image</span>
            </a>
          </div>
        </div>
        <p className="mt-2 text-xs text-center sm:text-left cursor-default">
          Use a managed cloud environment or self-deploy and run your own
          workspace.
        </p>
      </div>
      <div className="mx-auto w-full lg:w-5/6">
        <Section
          bgClass="bg-blue-900 border-b-0 border-x-0 "
          className="text-blue-50"
        >
          <ContentSection
            left={
              <div>
                <SectionTitle>
                  Use tools to build on top of open healthcare standards.
                </SectionTitle>
                <p className="mt-4 mb-8">
                  Create applications based on the most popular open standards
                  in healthcare.
                </p>
                <SectionContentContainer>
                  <SectionContent title="FHIR">
                    We have built GenFHI from the ground up around the open FHIR
                    standard. This allows you to have access to data from your
                    EHR and/or any FHIR based server, to seamlessly launch
                    applications within your EHR and to be able to utilize
                    shared logic and/or terminologies from third parties.
                  </SectionContent>
                  <SectionContent title="Ease of use">
                    FHIR is a powerful standard, but it is also difficult to
                    jump into as a developer. So, we've focused on making it
                    easy for developers to explore and build on top of FHIR.
                    This includes: autocompletion against your FHIR server's
                    capabilities; a powerful FHIRPath editor for users that
                    displays results, autocompletes variables, and has FHIRPath
                    syntax; and an intuitive drag-and-drop interface around
                    app-building and terminology editing.
                  </SectionContent>
                  <SectionContent title="Interoperability">
                    We are capable of interoperating with most EHRs and any
                    server based around the FHIR standard. Additionally, we are
                    able to automatically query your EHR and/or FHIR server to
                    determine their capabilities and show those capabilities as
                    you build your apps.
                  </SectionContent>
                </SectionContentContainer>
              </div>
            }
            right={
              <div className="flex items-center h-full">
                <div className="min-w-0 grid grid-cols-2  gap-2 items-center justify-center">
                  <ImageCard
                    image={smartAppBuilderScreenshot}
                    title={"SMART on FHIR App Builder"}
                    className="h-full w-full  "
                  />
                  <ImageCard
                    image={questionnaireScreenshot}
                    title="Questionnaire Builder"
                    className="h-full w-full "
                  />
                  <ImageCard
                    image={terminologyScreenshot}
                    title={"Terminology Editor"}
                    className="h-full w-full "
                  />

                  <ImageCard
                    image={endpointScreenShot}
                    title={"Endpoint Editor"}
                    className="h-full w-full "
                  />
                </div>
              </div>
            }
          />
        </Section>
        <Section className="text-blue-900">
          <ContentSection
            left={
              <div className="h-full flex justify-center items-center">
                <ImageCard
                  className="border-slate-900"
                  image={deploymentFeatureScreenshot}
                />
              </div>
            }
            right={
              <div>
                <SectionTitle>
                  Easily deploy healthcare applications.
                </SectionTitle>
                <p className="mt-4 mb-8">
                  GenFHI supports numerous options for deploying your
                  application into production.
                </p>
                <SectionContentContainer>
                  <SectionContent title="Cloud">
                    We can host your application on our servers. Choosing this
                    option will provide you with a shareable link. Additionally,
                    if you want a private link on your subnet, we can deploy
                    GenFHI as a Kubernetes cluster in your subnet.
                  </SectionContent>
                  <SectionContent title="Docker">
                    We can generate a single Docker image for your app. After
                    downloading, you can run your image on-premise or on top of
                    a cloud provider (AWS, GCP, Azure).
                  </SectionContent>
                  <SectionContent title="File system">
                    You can download your application as a JSON file and run it
                    using GenFHI's app runner image. This allows you to run
                    multiple applications from a single process.
                  </SectionContent>
                </SectionContentContainer>
              </div>
            }
          />
        </Section>
      </div>
    </div>
  );
}
