import React from "react";

import qBuilderImg from "../images/questionnaire-builder.png";
import qExpression1 from "../images/questionnaire-expression1.png";
import qExpression2 from "../images/questionnaire-expression2.png";
import qExpression3 from "../images/questionnaire-expression3.png";
import qExpression4 from "../images/questionnaire-expression4.png";
import qDnD from "../images/qdnd.gif";
import qApp from "../images/questionnaire-app.png";
import termQ from "../images/terminology-q.png";
import qAppBuilder from "../images/questionnaire-app-builder.png";

import { ImageCard } from "../components/ImageCard";
import {
  Section,
  ContentSection,
  SectionTitle,
  SectionContent,
  SectionContentContainer,
} from "../components/Section";
import { VideoHeader } from "../components/VideoHeader";

export default function QuestionnaireBuilder() {
  return (
    <div className="border-slate-900">
      <div className="mb-16">
        <VideoHeader
          headerClass="!text-blue-900"
          title="Build and deploy FHIR Questionnaires."
          subTitle={
            <div>
              <span>
                Quickly and easily create and deploy FHIR Questionnaires in
                minutes. {" "}
              </span>
              <span>
                See our{" "}
                <a
                  className="hover:text-orange-600 underline"
                  href="https://www.youtube.com/watch?v=DcsK0gogqcU"
                >
                  demo
                </a>
                .
              </span>
            </div>
          }
          imageSrc={qBuilderImg}
        />
      </div>
      <div className="mx-auto w-full lg:w-5/6">
        <Section
          bgClass="bg-blue-900 border-b-0 border-x-0 "
          className="text-blue-50"
        >
          <ContentSection
            title="Use tools to build on top of open healthcare standards."
            right={
              <div className="flex items-center h-full">
                <ImageCard image={qDnD} />
              </div>
            }
            left={
              <div>
                <SectionTitle>
                  Create Questionnaires with an intuitive interface.
                </SectionTitle>
                <p className="mt-4 mb-8">
                  Our Questionnaire builder embraces WYSIWYG (what you see is
                  what you get).
                </p>
                <SectionContentContainer>
                  <SectionContent title="Drag and Drop">
                    We support a full drag-and-drop interface. You can move
                    questions and add new questions by clicking and dragging
                    them into place. Properties for questions you click are
                    displayed on the right sidebar, allowing you to alter title,
                    prefix, etc., or more advanced properties like SDC
                    expressions.
                  </SectionContent>
                  <SectionContent title="Load data">
                    Through our app builder, we allow you to easily test
                    Questionnaires you create. This allows you to test your
                    questionnaire with data from your servers and/or easily
                    publish your Questionnaire into production.
                  </SectionContent>
                  <SectionContent title="Export">
                    We follow the SDC specification and the FHIR R4 version of
                    Questionnaire. We are compatible with other Questionnaire
                    renderers and allow you to easily export your Questionnaire.
                    Additionally, you can import or load Questionnaires
                    dynamically from your own FHIR server.
                  </SectionContent>
                </SectionContentContainer>
              </div>
            }
          />
        </Section>
        <Section className="text-blue-900">
          <ContentSection
            left={
              <div className="flex items-center h-full">
                <div className="min-w-0 grid grid-cols-2  gap-2 items-center justify-center">
                  <ImageCard
                    title="Syntax highlighting"
                    image={qExpression1}
                    className="h-full w-full  "
                  />
                  <ImageCard
                    title="Auto-completion"
                    image={qExpression2}
                    className="h-full w-full  "
                  />
                  <ImageCard
                    title="Display errors"
                    image={qExpression3}
                    className="h-full w-full  "
                  />
                  <ImageCard
                    title="Display results"
                    image={qExpression4}
                    className="h-full w-full  "
                  />
                </div>
              </div>
            }
            right={
              <div>
                <SectionTitle>Support for calculated expressions.</SectionTitle>
                <p className="mt-4 mb-8">
                  We support several options in the SDC specification. Such as
                  enablewhen expressions and calculated expressions.
                </p>
                <SectionContentContainer>
                  <SectionContent title="Auto-completion">
                    We support auto-completion for linkIds, properties, fhirpath
                    functions, and special variables. This allows you to quickly
                    build complex expressions without having to look up what's
                    available.
                  </SectionContent>
                  <SectionContent title="Fast evaluation">
                    Expressions will only run when their dependencies change.
                    This allows our questionnaires to run quickly and to only
                    re-evaluate expressions when needed.
                  </SectionContent>
                  <SectionContent title="Syntax highlighting">
                    Our editor uses custom syntax highlighting designed around
                    FHIRPath. This allows for easier readability and
                    comprehension when editing expressions.
                  </SectionContent>
                </SectionContentContainer>
              </div>
            }
          />
        </Section>
        <Section
          bgClass="bg-blue-900 border-b-0 border-x-0 "
          className="text-blue-50"
        >
          <ContentSection
            right={
              <div className="flex items-center h-full">
                <ImageCard
                  title="Application with inlined Questionnaire and dynamically chosen Questionnaire"
                  className="aspect-video w-full  "
                  image={qApp}
                />
              </div>
            }
            left={
              <div>
                <SectionTitle>Easily deploy Questionnaires.</SectionTitle>
                <p className="mt-4 mb-8">
                  We support several authentication methods and deployment
                  options for Questionnaires.
                </p>
                <SectionContentContainer>
                  <SectionContent title="Deployment options">
                    <ul className="list-outside list-disc space-y-2">
                      <li>
                        <span className="font-bold ">Public cloud</span>
                        <div>
                          <span>
                            We can host your Questionnaires on our servers.
                            Choosing this option will provide you with a
                            shareable public link.
                          </span>
                        </div>
                      </li>
                      <li>
                        <span className="font-bold ">Local subnet</span>
                        <div>
                          <span>
                            We can deploy GenFHI as a Kubernetes cluster in your
                            subnet. Allowing you to publish Questionnaires that
                            are only accessible from your network.
                          </span>
                        </div>
                      </li>
                      <li>
                        <span className="font-bold ">
                          Stand-alone docker image
                        </span>
                        <div>
                          <span>
                            We can generate a runnable Docker image for your
                            Questionnaire.
                          </span>
                        </div>
                      </li>
                      <li>
                        <span className="font-bold ">File system</span>
                        <div>
                          <span>
                            Download and run your application from the
                            filesystem using our app-runner image.
                          </span>
                        </div>
                      </li>
                    </ul>
                  </SectionContent>
                  <SectionContent title="Supported Auth">
                    <ul className="list-outside list-disc space-y-2">
                      <li>
                        <span className="font-bold ">
                          SMART on FHIR Standalone launch
                        </span>
                        <div>
                          <span>
                            SMART on FHIR flow for connecting to a FHIR server
                            outside of an EHR.
                          </span>
                        </div>
                      </li>
                      <li>
                        <span className="font-bold ">
                          SMART on FHIR EHR launch
                        </span>
                        <span>
                          <div>
                            <span>
                              Launching a SMART on FHIR app from within an EHR.
                            </span>
                          </div>
                        </span>
                      </li>
                      <li>
                        <span className="font-bold ">
                          OAuth2 Confidential Client
                        </span>
                        <div>
                          <span>
                            OAuth server side flow that uses a trusted
                            credential.
                          </span>
                        </div>
                      </li>
                      <li>
                        <span className="font-bold ">Basic Authentication</span>
                        <div>
                          <span>
                            HTTP authentication where user specifies credentials
                            in a request header field.
                          </span>
                        </div>
                      </li>
                    </ul>
                  </SectionContent>
                  <SectionContent title="Multiple Environments">
                    GenFHI has support for testing and deploying in multiple
                    environments. You can test in a development environment for
                    your Questionnaries and in a single click switch to
                    deploying and/or testing in production.
                  </SectionContent>
                </SectionContentContainer>
              </div>
            }
          />
        </Section>
        <Section className="text-blue-900">
          <ContentSection
            left={
              <div className="flex items-center h-full">
                <div className="min-w-0 grid grid-cols-1 xl:grid-cols-2 gap-2 items-center justify-center">
                  <ImageCard
                    title="Questionnaires in app building"
                    className="aspect-video w-full"
                    image={qAppBuilder}
                  />
                  <ImageCard
                    title="CodeSystem editor"
                    className="aspect-video w-full  "
                    image={termQ}
                  />
                </div>
              </div>
            }
            right={
              <div>
                <SectionTitle>
                  Integration with our other services.
                </SectionTitle>
                <p className="mt-4 mb-8">
                  We have tools for building FHIR terminology resources,
                  including SMART apps and tying FHIR server queries to
                  questionnaires. All of these services can be used in
                  conjunction with your Questionnaires.
                </p>
                <SectionContentContainer>
                  <SectionContent title="App building">
                    You can embed multiple Questionnaires you've made into our
                    app builder and pull in Questionnaires from your remote FHIR
                    server.
                  </SectionContent>
                  <SectionContent title="Terminology">
                    You can create answer ValueSets using our Terminology editor
                    and use them within your Questionnaires. We support
                    extensions for associating ordinal values.
                  </SectionContent>
                  <SectionContent title="Queries">
                    We support pulling and using Questionaires from remote
                    servers and submitting QuestionnaireResponses to remote FHIR
                    servers. Our tooling also helps by doing autocompletion for
                    queries based off a remote FHIR servers CapabilityStatement.
                  </SectionContent>
                </SectionContentContainer>
              </div>
            }
          />
        </Section>
      </div>
    </div>
  );
}
