import React, { Fragment, useState, useEffect } from "react";
import {
  BrowserRouter,
  Routes,
  Route,
  Link,
  useNavigate,
  useLocation
} from "react-router-dom";

import icon from "./images/icon.svg";

import { DropDown } from "./components/DropDown";

import TermsOfService from "./views/terms-of-service";
import PrivacyPolicy from "./views/privacy-policy";
import Home from "./views/Home";
import QuestionnaireBuilder from "./views/QuestionnaireBuilder";

function MainMenu({ children }) {
  const [showMobileMenu, setShowMobileMenu] = useState(false);
  const [scrollPos, setScrollPos] = useState(0);
  const navigate = useNavigate();
  const location = useLocation();
  console.log(location)
  const borderClass =
    scrollPos > 20 ? "drop-shadow-sm border-slate-200" : "border-transparent";
  useEffect(() => {
    const onScroll = (_e) => {
      setScrollPos(document.documentElement.scrollTop);
    };
    window.addEventListener("scroll", onScroll);

    return () => window.removeEventListener("scroll", onScroll);
  }, [scrollPos]);

  return (
    <header
      id="home"
      className={`transition sticky top-0 z-50 bg-white border border-t-0 border-r-0 border-l-0 ${borderClass}`}
    >
      <div className="px-4">
        <div className="flex items-center justify-between">
          <a className="flex py-2 items-center space-x-4" href="/">
            <img src={icon} className="w-12 h-12" />
            <h1 className="text-3xl font-bold hover:text-orange-900 ">GenFHI</h1>
          </a>
          <nav className="hidden md:flex flex-wrap justify-center space-x-2 font-bold">
            <DropDown
              items={[
                {
                  name: "SMART on FHIR App Builder",
		  active: (location.pathname == "/"),
                  onClick: (e) => {
                    navigate("");
                  },
                },
                {
                  name: "Questionnaire Builder",
		  active: (location.pathname == "/questionnaire"),
                  onClick: (e) => {
                    navigate("/questionnaire");
                  },
                },
                //{ name: "Terminology Editor" },
              ]}
            >
              Products
            </DropDown>
            <a
              target="_blank"
              className="px-4 py-2 rounded-md hover:text-orange-900 "
              href="https://docs.genfhi.app"
            >
              Docs
            </a>
            <a
              href="https://gitlab.com/genfhi/genfhi"
              target="_blank"
              className="px-4 py-2 rounded-md hover:text-orange-900 "
            >
              Source
            </a>
            <a
              href="mailto:business@genfhi.app"
              className="px-4 py-2 rounded-md hover:text-orange-900"
            >
              Contact
            </a>
            <a
              className="px-4 py-2 rounded-md hover:bg-orange-700 bg-orange-600  text-orange-50 "
              href="https://workspace.genfhi.app"
              target="_blank"
            >
              Sign in
            </a>
          </nav>
          <div className="block md:hidden relative z-20">
            <button
              onClick={() => setShowMobileMenu(!showMobileMenu)}
              className="p-2 rounded-md hover:bg-orange-100"
              id="headlessui-popover-button-1"
              type="button"
              aria-expanded="false"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
                className="w-5 h-5 text-slate-700"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="3"
                  d="M4 6h16M4 12h16M4 18h16"
                ></path>
              </svg>
            </button>
            {showMobileMenu && (
              <div>
                <div className="absolute right-0 bg-white w-64 p-2 rounded-md shadow-lg">
                  <nav className="flex flex-col space-y-1 font-bold">
                    <a
                      className="p-2 rounded-lg hover:bg-orange-50  text-slate-600 hover:text-slate-900 "
                      href="/"
                    >
                      Home
                    </a>
                    <a
                      className="p-2 rounded-lg hover:bg-orange-50  text-slate-600 hover:text-slate-900 "
                      href="/#features"
                    >
                      Features
                    </a>
                    <a
                      href="https://gitlab.com/genfhi/genfhi"
                      target="_blank"
                      className="p-2 rounded-lg hover:bg-orange-50  text-slate-600 hover:text-slate-900 "
                    >
                      Source
                    </a>

                    <a
                      href="mailto:business@genfhi.app"
                      target="_blank"
                      className="p-2 rounded-lg hover:bg-orange-50  text-slate-600 hover:text-slate-900 "
                    >
                      Contact
                    </a>
                  </nav>
                </div>
              </div>
            )}
          </div>
        </div>
        {children}
      </div>
    </header>
  );
}

function App() {
  return (
    <>
      <BrowserRouter>
        <MainMenu />
        <div class="bg-white p-8">
          <Routes>
            <Route path="/">
              <Route index element={<Home />} />
              <Route path="privacy" element={<PrivacyPolicy />} />
              <Route path="terms" element={<TermsOfService />} />
              <Route path="questionnaire" element={<QuestionnaireBuilder />} />
            </Route>
          </Routes>
        </div>
        <footer className="text-slate-900 w-full mx-auto px-4 mb-8">
          <nav className="flex items-center justify-center">
            <div className="px-4 py-2">
              <a href="/#home" className="hover:text-orange-900">
                Home
              </a>
            </div>

            <div className="px-4 py-2">
              <a
                href="https://gitlab.com/genfhi/genfhi/container_registry/2417387"
                className="hover:text-orange-900"
              >
                Download Image
              </a>
            </div>
            <div className="px-4 py-2">
              <Link
                to="/privacy"
                onClick={(e) => {
                  // Scroll back to top.
                  window.scrollTo(0, 0);
                }}
                className="hover:text-orange-900"
              >
                Privacy Policy
              </Link>
            </div>
            <div className="px-4 py-2">
              <Link
                to="/terms"
                className="hover:text-orange-900"
                onClick={(e) => {
                  // Scroll back to top.
                  window.scrollTo(0, 0);
                }}
              >
                Terms of Service
              </Link>
            </div>
          </nav>
        </footer>
      </BrowserRouter>
    </>
  );
}

export default App;
