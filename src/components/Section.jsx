import React from "react";

function BackgroundMask({ style, className, children }) {
  return (
    <div
      style={{ ...style }}
      className={`border border-dashed border-t-0 absolute w-full h-full -skew-y-3 p-20 overflow-x-hidden font-semibold ${className}`}
    >
      <div className={`mx-auto py-4 sm:pb-8 relative skew-y-3`}>{children}</div>
    </div>
  );
}

export function SectionContentContainer({ children }) {
  return (
    <div className="grid grid-cols-1 lg:grid-cols-2 xl:grid-cols-3 text-base">
      {children}
    </div>
  );
}

export function SectionContent({ title, children }) {
  return (
    <div className="mb-4 mr-4">
      <div>
        <div className="underline mb-2 text-xl font-bold">{title}</div>
        <p>{children}</p>
      </div>
    </div>
  );
}

export function Section({ style, className, children, bgClass }) {
  return (
    <div className=" relative">
      <BackgroundMask className={bgClass} />
      <div style={style} className={`px-8 py-24 ${className}`}>
        <div className={`mx-auto relative`}>{children}</div>
      </div>
    </div>
  );
}

export function SectionTitle({ children }) {
  return (
    <div>
      <h3 className="break-words text-5xl m-0 p-0 font-bold">{children}</h3>
    </div>
  );
}

export function ContentSection({ left, right }) {
  return (
    <div id="features" className="mt-8 mx-auto grid grid-cols-1 lg:grid-cols-2">
      <div className="mb-12  md:mr-12">{left}</div>
      <div>{right}</div>
    </div>
  );
}
