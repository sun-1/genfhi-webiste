import React from "react";

export function VideoHeader({ headerClass, title, subTitle, videoSrc, imageSrc }) {
  return (
    <div className="w-full flex justify-center items-center flex-col space-y-8 md:space-y-0">
      <div className="flex-none items-center flex flex-col w-full md:w-3/4">
        <div className={`w-full mb-4 flex flex-col ${headerClass}`}>
          <h1 className={`break-words text-6xl font-bold cursor-default `}>
            {title}
          </h1>
          <p className="text-xl  mt-4 cursor-default font-semibold">
            {subTitle}
            <br />
          </p>
        </div>
        <div className="flex-1 min-w-0 w-full flex items-center justify-center mt-4 aspect-video">
          {imageSrc ? (
            <img className="h-full w-full" src={imageSrc} />
          ) : (
            <iframe
              width="100%"
              height="100%"
              src={videoSrc}
              title="YouTube video player"
              frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              allowfullscreen
            ></iframe>
          )}
        </div>
      </div>
    </div>
  );
}
