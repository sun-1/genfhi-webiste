import React, { useState, useEffect } from "react";

export function TextReader({ text, speed, className, style }) {
  const [display, setDisplay] = useState({ text, curPosition: 1 });

  useEffect(() => {
    const intervalID = setInterval(() => {
      setDisplay((display) => {
        let nextPosition = display.curPosition + 1;
        while (text.charAt(nextPosition) == " ".charAt(0)) {
          nextPosition += 1;
        }
        if (nextPosition >= text.length) {
          clearInterval(intervalID);
        }
        return {
          curPosition: nextPosition,
          textDisplay: text.substring(0, nextPosition),
        };
      });
    }, speed);

    return () => {
      clearInterval(intervalID);
    };
  }, [text, speed]);

  return (
    <span className={className} style={style}>
      {text.substring(0, display.curPosition)}
      <span className="text-transparent">
        {text.substring(display.curPosition, text.length)}
      </span>
    </span>
  );
}
