import React, {Fragment} from "react"
import { Menu, Transition } from "@headlessui/react";

export function DropDown({ children, items }) {
  return (
    <Menu as="div" className="relative inline-block text-left">
      <div>
        <Menu.Button className="px-4 py-2 rounded-md hover:text-orange-900 font-semibold">
          {children}
        </Menu.Button>
      </div>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="absolute right-0 mt-2 w-56 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
          <div className="px-1 py-1 ">
            {items.map((item) => (
              <Menu.Item>
                {({ active }) => (
                  <button
                    onClick={item.onClick ? item.onClick : () => {}}
                    className={`${ item.active ? "bg-orange-600 text-white" :
active ? "text-orange-600" : "text-slate-900"
                    } group flex w-full items-center px-2 py-2 text-sm font-semibold`}
                  >
                    {item.name}
                  </button>
                )}
              </Menu.Item>
            ))}
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  );
}
