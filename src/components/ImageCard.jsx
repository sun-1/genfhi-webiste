import React from "react";

export function ImageCard({ style, title, image, className }) {
  return (
    <div
      style={style}
      className={`${
        className || ""
      } bg-orange-50 text-orange-900 rounded-lg overflow-hidden border relative drop-shadow-lg`}
    >
      {title && <div className="border-x-0 border-t-0 border font-bold p-1 px-2">{title}</div>}
      <img className="h-full w-full object-cover" src={image} />
    </div>
  );
}
