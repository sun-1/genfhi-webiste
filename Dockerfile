FROM node:16-alpine as node
COPY . /site
WORKDIR /site
RUN ["npm", "i"]
RUN ["npm", "run", "build"]

FROM nginx
EXPOSE 80
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=node /site/build /usr/share/nginx/html